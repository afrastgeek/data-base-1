# Presentation Result

## General
- First to come up, any problem?
- 10 Minutes? Is it enough? Effectifity! (skip on unneeded content)
- use smartphone, nice strategy! jaga kontak mata dengan slide.
- to be the best: be complete, be unique, be the first.

Diharapkan menemukan real problem. dan dapat mengembangkan skill masing-masing.

## K1 - KAA
pros:
- konteks lengkap
- kendala spesifik di lapangan
- penyampaian bagus, konten yang hanya akan didapat ketika berkunjung.

cons:
- isi slide lebih ringkas
- query informatif

## K2 - KBB
pros:
- gratis.
- disampaikan kasus spesifik di lapangan.
- penyampaian cukup baik.
- petugas berhubungan dengan kandang??.

cons:
- problem hubungkan dengan materi basis data (mis: usia => early warning).

## K3 - MPI
pros:
- presentasinya baik (slide & talks).
- good model for presentation so far!
- few data, fewer to represent.
- query lengkap.
- kemampuan kelompok merata.

cons:
- "datanya hanya ini". don't bring your problem, bring your ideas.

## K4 - Bosscha
pros:
- menjiwai peran. sampai hafal nama objek.
- topik yang diangkat unik.
- mengambil spesifik koleksi tentang teropong. DECISION!
- query ok!

## K5 - Barli
pros:
- slide paling siap. sepele tapi dampaknya besar.
- font, mudah terbaca.
- pembahasan efektif.
- ada yang hafal.

cons:
- tidak semua anggota percaya diri.
- query masih bersifat dasar.

## K6 - Sri Baduga
pros:
- meski ada kendala yang tidak dapat dihindari, di akhir ada data yang ditampilkan.
- penjelasan baik, namun persiapannya (slide) kurang.
- kendala diangkat menjadi image.

cons:
- harus lebih siap, apapun datanya.

## K7 - MuPeNas
pros:
- data manual. normalisasi jadi wajib.
- koleksi detail. bahkan memisahkan bahan hingga levelnya.
- analisa.

cons:
- slide tidak terbaca.

## K8 - Mandala Wangsit Siliwangi
pros:
- pengelompokkan jenis koleksi ok.
- menggunakan bantuan hp untuk presentasi.
- muncul entitas baru yang bukan template dari museum lain.
- benchmarking ke role model.

## K9 - Geologi
pros:
- presentasi ok, tenang, jenaka.
- analisa logikanya baik.
- query ok.
- pembagian kelompoknya baik.

cons:
- hati-hati dengan waktu.
