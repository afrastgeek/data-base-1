## Syllabus

- DDL
- DML
- Agregasi dan Grouping
- Join
- SubQuery
- Aljabar Relasional
- Power Designer


## Scoring Rules
| Point | Value |
|-------|-------|
|Presensi|10%|
|UTS|25%|
|UAS|25%|
|TP|20%|
|Kuis|20%|