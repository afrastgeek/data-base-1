create database kuliah;
    use kuliah;

    create table mahasiswa(
        nim varchar(10) Primary Key,
        nama varchar(25) NOT NULL,
        jurusan varchar(30) NOT NULL
        );

    alter table mahasiswa rename to mhs;
    alter table mhs add column (usia varchar(5) NOT NULL);
    alter table mhs modify usia int;
    alter table mhs drop column usia;

    drop table mhs;
    drop database kuliah;

    create table matkul(
        kd_matkul varchar(10) primary key,
        nama_matkul varchar(25) not null
        );

    create table kontrak(
        id_kontrak int primary key,
        nim varchar(10) not null,
        kd_matkul varchar(10) not null,
        foreign key(nim) references mhs(nim),
        foreign key(kd_matkul) references matkul(kd_matkul)
        );